from django.conf.urls import url, include
from django.contrib import admin
import lab_1.urls as lab_1
import lab_2.urls as lab_2
import lab_3.urls as lab_3
import lab_4.urls as lab_4
import lab_5.urls as lab_5
import lab_6.urls as lab_6
import lab_7.urls as lab_7
import lab_8.urls as lab_8
import lab_9.urls as lab_9
from lab_1.views import index as index_lab1
from lab_2.views import index as index_lab2
from lab_3.views import index as index_lab3
from lab_4.views import index as index_lab4
from lab_5.views import index as index_lab5
from lab_6.views import index as index_lab6
from lab_7.views import index as index_lab7
from lab_8.views import index as index_lab8
from lab_9.views import index as index_baru

urlpatterns = [
 url(r'^admin/', admin.site.urls),
 url(r'^lab-1/', include(lab_1,namespace='lab-1')),
 url(r'^lab-2/', include(lab_2,namespace='lab-2')),
 url(r'^lab-3/', include(lab_3,namespace='lab-3')),
 url(r'^lab-4/', include(lab_4,namespace='lab-4')),
 url(r'^lab-5/', include(lab_5,namespace='lab-5')),
 url(r'^lab-6/', include(lab_6,namespace='lab-6')),
 url(r'^lab-7/', include(lab_7,namespace='lab-7')),
 url(r'^lab-8/', include(lab_8,namespace='lab-8')),
 url(r'^lab-9/', include(lab_9,namespace='lab-9')),
 url(r'^$', index_baru, name='index')
]
